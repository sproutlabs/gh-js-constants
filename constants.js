// https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Data.md#object
// / See http://xapi.vocab.pub/datasets/adl/#question and https://registry.tincanapi.com for more
module.exports = {
    workflows: {
        XAPI_STATEMENT: 'XAPI_STATEMENT',
        EXACT_MATCH: 'EXACT_MATCH',
        SEND_EMAIL: 'SEND_EMAIL',
        AWARD_ACHIEVEMENT: 'AWARD_ACHIEVEMENT',
        AND: 'AND',
        selectors: {
            verb: 'verb.id',
            objectId: 'object.id',
            interactionType: 'object.interactionType',
            response: 'result.response'
        },
        ASSIGN_LEARNER_TASK: 'ASSIGN_LEARNER_TASK',
        ASSIGN_OBJECT: 'ASSIGN_OBJECT',
        PREVIOUS_XAPI_STATEMENT: 'PREVIOUS_XAPI_STATEMENT',
    },
    xapi: {
        verbs: {
            completed: {
                id: 'http://adlnet.gov/expapi/verbs/completed',
                display: {'en-US': 'completed'},
            },
            answered: {
                id: 'http://adlnet.gov/expapi/verbs/answered',
                display: {'en-US': 'answered'},
            },
            disagreed: {
                id: 'http://activitystrea.ms/schema/1.0/disagree',
                display: {'en-US': 'disagreed'},
            },
            agreed: {
                id: 'http://activitystrea.ms/schema/1.0/agree',
                display: {'en-US': 'agreed'},
            },
            accessed: {
                id: 'http://activitystrea.ms/schema/1.0/access',
                display: {'en-US': 'accessed'},
            },
            assigned: {
                id: 'http://activitystrea.ms/schema/1.0/assign',
                display: {'en-US': 'assigned'},
            },
            opened: {
                id: 'http://activitystrea.ms/schema/1.0/open',
                display: {'en-US': 'opened'},
            },
            played: {
                id: 'http://activitystrea.ms/schema/1.0/play',
                display: {'en-US': 'played'},
            },
            mediaPaused: {
                id: 'http://id.tincanapi.com/verb/paused',
                display: {'en-US': 'paused'},
            },
            watched: {
                id: 'http://activitystrea.ms/schema/1.0/watch',
                display: {'en-US': 'watched'},
            },
            edited: {
                id: 'http://curatr3.com/define/verb/edited',
                display: {'en-US': 'edited'},
            },
            pickedUp: {
                id: 'http://getglasshouse.com/verb/pickedup',
                display: {'en-US': 'picked up'},
            },
            dropped: {
                id: 'http://getglasshouse.com/verb/dropped',
                display: {'en-US': 'dropped'},
            },
            viewed: {
                id: 'http://id.tincanapi.com/verb/viewed',
                display: {'en-US': 'viewed'},
            },
            earned: {
                id: 'http://id.tincanapi.com/verb/earned',
                display: {'en-US': 'earned'},
            },
            started: {
                id: 'http://activitystrea.ms/schema/1.0/start',
                display: {'en-US': 'started'},
            },
            failed: {
                id: 'http://adlnet.gov/expapi/verbs/failed',
                display: {'en-US': 'failed'}
            },
            attempted: {
                id: 'http://adlnet.gov/expapi/verbs/attempted',
                display: {'en-US': 'attempted'}
            },
            submitted: {
                id: 'http://activitystrea.ms/schema/1.0/submit',
                display: {'en-US': 'submitted'}
            },
            scored: {
                id: 'http://adlnet.gov/expapi/verbs/scored',
                display: {'en-US': 'scored'}
            },

        },
    },
};
